<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">

    <title>Form Penjadwalan Dosen</title>
  </head>
  <body>
    <h1>Penjadwalan Dosen</h1>
    <?php
        $Nama_Dosen = $_POST["Nama_Dosen"];
        $Fakultas = $_POST["Fakultas"];
        $Kelas_Dosen = $_POST["Kelas_Dosen"];
        $Jadwal = $_POST["Jadwal"];
        $Mata_kuliah = $_POST["Mata_kuliah"];
        $NIP = $_POST["NIP"];
        $Prodi = $_POST["Prodi"];

        include "koneksidata.php";

        $sqldosen = "INSERT INTO dosen(nip_dosen,nama_dosen,prodi,fakultas) values('".$NIP."','".$Nama_Dosen."','".$Prodi."','".$Fakultas."')";
        $sqljadwalkelas = "INSERT INTO jadwalkelas(jadwal,mata_kuliah) values('".$Jadwal."','".$Mata_kuliah."')";
        $sqlkelas = "INSERT INTO kelas(nama_kelas,prodi,fakultas) values('".$Kelas_Dosen."','".$Prodi."','".$Fakultas."')";

        if(mysqli_query($conn, $sqldosen)){
          echo "Berhasil Menyimpan Data";
        } else{
          echo "Gagal Menyimpan data";
        }

        if(mysqli_query($conn, $sqljadwalkelas)){
          echo "Berhasil Menyimpan Data";
        } else{
          echo "Gagal Menyimpan data";
        }

        if(mysqli_query($conn, $sqlkelas)){
          echo "Berhasil Menyimpan Data";
        } else{
          echo "Gagal Menyimpan data";
        }
    
    ?>

  </body>
  <div>
    <table class="table">
    <thead> <!--- di   posisi table di atas --->
  <th>Perihal</th>
  <th>Masukan</th>
  </thead>  
  <tbody> <!--- --->
      <th scope="row">Nama Dosen</th> <!--- tag yang digunakan untuk membuat baris header tabel--->
      <td colspan="1" class="table-active"><?php echo $Nama_Dosen ?> </td>
    </tr>

    <tr> <!---  tag untuk membuat baris tabel--->
      <th scope="row">Fakultas</th> 
      <td colspan="2" class="table-active"><?php echo $Fakultas ?> </td> 
    </tr>

    <tr>
      <th scope="row">Kelas Dosen</th>
      <td colspan="2" class="table-active"><?php echo $Kelas_Dosen ?> </td>
    </tr>

    <tr>
      <th scope="row">Jadwal</th>
      <td colspan="2" class="table-active"><?php echo $Jadwal ?> </td>
    </tr>

    <tr>
      <th scope="row">Mata_kuliah</th>
      <td colspan="2" class="table-active"><?php echo $Mata_kuliah ?> </td>
    </tr>

    <tr>
      <th scope="row">NIP</th>
      <td colspan="2" class="table-active"><?php echo $NIP ?> </td>
    </tr>

    <tr>
      <th scope="row">Prodi</th>
      <td colspan="2" class="table-active"><?php echo $Prodi ?> </td>
    </tr>

  </tbody>
</table>

<br>
  </html>